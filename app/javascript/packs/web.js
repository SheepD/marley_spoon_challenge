import Vue from 'vue'
import App from '../app.vue'
import router from '../router'

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    render: h => h(App),
    router
  }).$mount()
  document.body.appendChild(app.$el)
})
