import Vue from 'vue'
import VueRouter from 'vue-router'

import RecipeIndex from './views/RecipeIndex'
import RecipeDetail from './views/RecipeDetail'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: RecipeIndex
  },
  {
    path: '/recipe',
    name: 'recipeIndex',
    component: RecipeIndex
  },
  {
    path: '/recipe/:id',
    name: 'recipeDetail',
    component: RecipeDetail
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
