module Contentful
  class RecipeEntry
    def initialize(deps={})
      @client = deps[:client] || Client.new
      @uri_builder = deps[:uri_builder] || UriBuilder.new
      @presenter = deps[:presenter] || Presenter::Show.new
    end

    def call(entry_id)
      uri = @uri_builder.entry_uri(entry_id)
      response = @client.call(uri)
      @presenter.call(response)
    end
  end
end
