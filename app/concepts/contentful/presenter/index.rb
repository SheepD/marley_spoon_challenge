module Contentful
  module Presenter
    class Index
      def call(response)
        items = response['items']
        assets = response['includes']['Asset']

        items.map do |item|
          photo_id = item['fields']['photo']['sys']['id']
          photo = assets.find { |a| a['sys']['id'] == photo_id }

          {
            id: item['sys']['id'],
            title: item['fields']['title'],
            image: photo['fields']['file']['url']
          }
        end
      end
    end
  end
end
