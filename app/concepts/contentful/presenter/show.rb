module Contentful
  module Presenter
    class Show
      def call(response)
        item = response['items'].first
        assets = response['includes']['Asset']
        entries = response['includes']['Entry']

        # TODO: DRY this logic with index presenter
        photo_id = item['fields']['photo']['sys']['id']
        photo = assets.find { |a| a['sys']['id'] == photo_id }

        if item['fields']['chef']
          chef_id = item['fields']['chef']['sys']['id']
          chef = entries.find { |e| e['sys']['id'] == chef_id }['fields']['name']
        end

        if item['fields']['tags']
          tag_ids =
            item['fields']['tags'].map do |tag|
              tag['sys']['id']
            end

          tags =
            entries.select do |entry|
              tag_ids.include?(entry['sys']['id'])
            end.map do |tag|
              tag['fields']['name']
            end
        end

        {
          id: item['sys']['id'],
          title: item['fields']['title'],
          image: photo['fields']['file']['url'],
          tags: tags,
          description: item['fields']['title'],
          chef: chef
        }
      end
    end
  end
end
