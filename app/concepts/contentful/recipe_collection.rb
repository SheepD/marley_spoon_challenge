module Contentful
  class RecipeCollection
    def initialize(deps={})
      @client = deps[:client] || Client.new
      @uri_builder = deps[:uri_builder] || UriBuilder.new
      @presenter = deps[:presenter] || Presenter::Index.new
    end

    def call
      uri = @uri_builder.entry_collection_uri
      response = @client.call(uri)
      @presenter.call(response)
    end
  end
end