module Contentful
  class Client
    def initialize(deps = {})
      @http_client = deps[:http_client] || Net::HTTP
      @json_lib = deps[:json_lib] || JSON
    end

    def call(uri)
      response = @http_client.get(uri)
      @json_lib.parse(response)
    end
  end
end
