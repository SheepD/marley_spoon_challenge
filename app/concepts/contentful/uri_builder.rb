module Contentful
  class UriBuilder
    def initialize(deps={})
      @content_type = deps[:content_type] || ENV.fetch('CONTENTFUL_RECIPE_CONTENT_TYPE')
      @api_endpoint = deps[:api_endpoint] || ENV.fetch('CONTENTFUL_API_ENDPOINT')
      @space_id = deps[:space_id] || ENV.fetch('CONTENTFUL_SPACE_ID')
      @environment = deps[:environment] || ENV.fetch('CONTENTFUL_ENV')
      @api_key = deps[:api_key] || ENV.fetch('CONTENTFUL_API_KEY')
    end

    def entry_collection_uri
      URI("https://#{@api_endpoint}/spaces/#{@space_id}/environments/#{@environment}/entries?content_type=#{@content_type}&access_token=#{@api_key}")
    end

    def entry_uri(entry_id)
      URI("https://#{@api_endpoint}/spaces/#{@space_id}/environments/#{@environment}/entries?content_type=#{@content_type}&access_token=#{@api_key}&sys.id=#{entry_id}")
    end
  end
end
