class WebController < ApplicationController
  skip_before_action :verify_authenticity_token, only: %i[not_found]
  def index; end

  def not_found
    head :not_found
  end
end
