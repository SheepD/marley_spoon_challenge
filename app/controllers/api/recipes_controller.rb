module Api
  class RecipesController < ApplicationController
    def index
      recipes = Contentful::RecipeCollection.new.call
      render json: recipes, status: :ok
    end

    def show
      recipe = Contentful::RecipeEntry.new.call params[:id]
      render json: recipe, status: :ok
    end
  end
end
