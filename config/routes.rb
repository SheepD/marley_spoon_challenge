Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'web#index'

  namespace :api do
    get 'recipes' => 'recipes#index'
    get 'recipes/:id' => 'recipes#show'
  end

  # send all non-api GET traffic to the front end app
  get '/*etc', to: 'web#index'

  # handle 404s
  post '/*etc', to: 'web#not_found'
  put '/*etc', to: 'web#not_found'
  patch '/*etc', to: 'web#not_found'
  delete '/*etc', to: 'web#not_found'
end
