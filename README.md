# Marley Spoon Web Challenge!

A simple web app that display items fetched from Contentful Delivery API

# Table of Contents
1. [Design Choices](#design-choices)
1. [Room for Improvement](#room-for-improvement)
1. [Setup](#setup)

# Design Choices

## Ruby on Rails
While it is possible to create everything purely on the front end(since I can just fetch directly from the contenful delivery api), I opted to use Ruby on Rails to showcase how I write Ruby code.

# Room for improvement
Due to time constraints I wasn't able to give the application the polish that is expected from a production ready application, but I will list the shortcomings i am aware of from the top of my head

## Tests
Tests that go beyond the happy path is something desirable as it gives us some assurance that the code behaves the way we think it does. It is also a wonderful safety net for when we refactor or change other aspects of the code.

## Better UI
My layout and the UI is pretty bland. Nothing fancy... I wish I had some extra time to simply make it stand out a bit more.

## CI/CD
Continuous Deployment is something common now in large organizations and I would love to have the time to set this up for this project

## Pagination
The contentful api has a limitation of 100 items per request. While the space i was provided only had four items, my code will not be able to handle items beyond 100

# Setup

## Required ENV variables
```
CONTENTFUL_RECIPE_CONTENT_TYPE=recipe
CONTENTFUL_API_ENDPOINT=cdn.contentful.com
CONTENTFUL_SPACE_ID=yourspaceid
CONTENTFUL_ENV=theenvironment
CONTENTFUL_API_KEY=yourkeys
```

## Docker
1. checkout the code
1. create a file `.env/development` and input the required ENV variables inside. see `.env/development.sample` for reference
1. `docker-compose up`
1. visit the site on `localhost:3000`

## Manual Setup

### Requirements
* Ruby 2.7
* NodeJS
* yarn

### Starting the app
1. checkout the code
1. set the environment variables noted in `.env/development.sample`
1. run `bundle install`
1. run `yarn`
1. in one terminal, start rails with `rails s`
1. in another temrinal, start the webpack dev server with `bin/webpack-dev-server`
1. visit the site on `localhost:3000`
